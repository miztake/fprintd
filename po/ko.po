# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Seong-ho Cho <darkcircle.0426@gmail.com>, 2013
# Seong-ho Cho <darkcircle.0426@gmail.com>, 2011
# Seong-ho Cho <darkcircle.0426@gmail.com>, 2011,2013
# Shinjo Park <kde@peremen.name>, 2015
# Seong-ho Cho <darkcircle.0426@gmail.com>, 2011,2013
# Shinjo Park <kde@peremen.name>, 2015
msgid ""
msgstr ""
"Project-Id-Version: fprintd\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-02-06 14:07+0100\n"
"PO-Revision-Date: 2017-09-19 09:46+0000\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean (http://www.transifex.com/freedesktop/fprintd/language/ko/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ko\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../data/net.reactivated.fprint.device.policy.in.h:1
msgid "Verify a fingerprint"
msgstr "지문 검증"

#: ../data/net.reactivated.fprint.device.policy.in.h:2
msgid "Privileges are required to verify fingerprints."
msgstr "지문을 검증하려면 권한이 필요합니다."

#: ../data/net.reactivated.fprint.device.policy.in.h:3
msgid "Enroll new fingerprints"
msgstr "새 지문 등록"

#: ../data/net.reactivated.fprint.device.policy.in.h:4
msgid "Privileges are required to enroll new fingerprints."
msgstr "새 지문을 등록하려면 권한이 필요합니다."

#: ../data/net.reactivated.fprint.device.policy.in.h:5
msgid "Select a user to enroll"
msgstr "등록할 사용자를 선택하십시오"

#: ../data/net.reactivated.fprint.device.policy.in.h:6
msgid "Privileges are required to enroll new fingerprints for other users."
msgstr "다른 사용자의 새 지문을 등록하려면 권한이 필요합니다."

#: ../src/device.c:385
#, c-format
msgid "Device was not claimed before use"
msgstr "장치 사용 전 사용 등록되지 않았음"

#: ../src/device.c:395
#, c-format
msgid "Device already in use by another user"
msgstr "다른 사용자가 장치 사용 중"

#: ../pam/fingerprint-strings.h:31
msgid "Place your finger on the fingerprint reader"
msgstr "지문 인식기에 손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:32
#, c-format
msgid "Place your finger on %s"
msgstr "%s에 손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:33
msgid "Swipe your finger across the fingerprint reader"
msgstr "지문 인식기에 손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:34
#, c-format
msgid "Swipe your finger across %s"
msgstr "%s에 손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:36
msgid "Place your left thumb on the fingerprint reader"
msgstr "왼손 엄지손가락을 지문 인식기에 올려놓으십시오"

#: ../pam/fingerprint-strings.h:37
#, c-format
msgid "Place your left thumb on %s"
msgstr "%s에 왼손 엄지손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:38
msgid "Swipe your left thumb across the fingerprint reader"
msgstr "왼손 엄지손가락을 지문 인식기에 문지르십시오"

#: ../pam/fingerprint-strings.h:39
#, c-format
msgid "Swipe your left thumb across %s"
msgstr "%s에 왼손 엄지손가락를 문지르십시오"

#: ../pam/fingerprint-strings.h:41
msgid "Place your left index finger on the fingerprint reader"
msgstr "지문 인식기에 왼손 집게손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:42
#, c-format
msgid "Place your left index finger on %s"
msgstr "%s에 왼손 집게손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:43
msgid "Swipe your left index finger across the fingerprint reader"
msgstr "지문 인식기에 왼손 집게손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:44
#, c-format
msgid "Swipe your left index finger across %s"
msgstr "%s에 왼손 집게손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:46
msgid "Place your left middle finger on the fingerprint reader"
msgstr "지문 인식기에 왼손 가운뎃손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:47
#, c-format
msgid "Place your left middle finger on %s"
msgstr "%s에 왼손 가운뎃손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:48
msgid "Swipe your left middle finger across the fingerprint reader"
msgstr "지문 인식기에 왼손 가운뎃손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:49
#, c-format
msgid "Swipe your left middle finger across %s"
msgstr "%s에 왼손 가운뎃손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:51
msgid "Place your left ring finger on the fingerprint reader"
msgstr "지문 인식기에 왼손 약손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:52
#, c-format
msgid "Place your left ring finger on %s"
msgstr "%s에 왼손 약손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:53
msgid "Swipe your left ring finger across the fingerprint reader"
msgstr "지문 인식기에 왼손 약손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:54
#, c-format
msgid "Swipe your left ring finger across %s"
msgstr "%s에 왼손 약손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:56
msgid "Place your left little finger on the fingerprint reader"
msgstr "지문 인식기에 왼손 새끼손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:57
#, c-format
msgid "Place your left little finger on %s"
msgstr "%s에 왼손 새끼손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:58
msgid "Swipe your left little finger across the fingerprint reader"
msgstr "지문 인식기에 왼손 새끼손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:59
#, c-format
msgid "Swipe your left little finger across %s"
msgstr "%s에 왼손 새끼손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:61
msgid "Place your right thumb on the fingerprint reader"
msgstr "지문 인식기에 오른손 엄지손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:62
#, c-format
msgid "Place your right thumb on %s"
msgstr "%s에 오른손 엄지손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:63
msgid "Swipe your right thumb across the fingerprint reader"
msgstr "지문 인식기에 오른손 엄지손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:64
#, c-format
msgid "Swipe your right thumb across %s"
msgstr "%s에 오른손 엄지손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:66
msgid "Place your right index finger on the fingerprint reader"
msgstr "지문 인식기에 오른손 집게손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:67
#, c-format
msgid "Place your right index finger on %s"
msgstr "%s에 오른손 집게손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:68
msgid "Swipe your right index finger across the fingerprint reader"
msgstr "지문 인식기에 오른손 집게손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:69
#, c-format
msgid "Swipe your right index finger across %s"
msgstr "%s에 오른손 집게손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:71
msgid "Place your right middle finger on the fingerprint reader"
msgstr "지문 인식기에 오른손 가운뎃손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:72
#, c-format
msgid "Place your right middle finger on %s"
msgstr "%s에 오른손 가운뎃손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:73
msgid "Swipe your right middle finger across the fingerprint reader"
msgstr "지문 인식기에 오른손 가운뎃손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:74
#, c-format
msgid "Swipe your right middle finger across %s"
msgstr "%s에 오른손 가운뎃손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:76
msgid "Place your right ring finger on the fingerprint reader"
msgstr "지문 인식기에 오른손 약손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:77
#, c-format
msgid "Place your right ring finger on %s"
msgstr "%s에 오른손 약손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:78
msgid "Swipe your right ring finger across the fingerprint reader"
msgstr "지문 인식기에 오른손 약손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:79
#, c-format
msgid "Swipe your right ring finger across %s"
msgstr "%s에 오른손 약손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:81
msgid "Place your right little finger on the fingerprint reader"
msgstr "지문 인식기에 오른손 새끼손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:82
#, c-format
msgid "Place your right little finger on %s"
msgstr "%s에 오른손 새끼손가락을 올려놓으십시오"

#: ../pam/fingerprint-strings.h:83
msgid "Swipe your right little finger across the fingerprint reader"
msgstr "지문 인식기에 오른손 새끼손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:84
#, c-format
msgid "Swipe your right little finger across %s"
msgstr "%s에 오른손 새끼손가락을 문지르십시오"

#: ../pam/fingerprint-strings.h:131 ../pam/fingerprint-strings.h:157
msgid "Place your finger on the reader again"
msgstr "인식기에 손가락을 다시 올려놓으십시오"

#: ../pam/fingerprint-strings.h:133 ../pam/fingerprint-strings.h:159
msgid "Swipe your finger again"
msgstr "인식기에 손가락을 다시 문지르십시오"

#: ../pam/fingerprint-strings.h:136 ../pam/fingerprint-strings.h:162
msgid "Swipe was too short, try again"
msgstr "문지르는 시간이 짧습니다. 다시 시도해 주십시오"

#: ../pam/fingerprint-strings.h:138 ../pam/fingerprint-strings.h:164
msgid "Your finger was not centered, try swiping your finger again"
msgstr "손가락이 가운데에 있지 않았습니다. 손가락을 인식기에 다시 문질러 보십시오"

#: ../pam/fingerprint-strings.h:140 ../pam/fingerprint-strings.h:166
msgid "Remove your finger, and try swiping your finger again"
msgstr "손가락을 뗀 다음 인식기에 다시 문질러 보십시오"
